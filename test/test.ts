import generateAuditRecord from '@digigov-oss/gsis-audit-record-db'
import {CouchDBEngine} from '../src/index';
import config from './config.json';

const test = async () => {
    const ar = await generateAuditRecord({}, new CouchDBEngine(config.connection,{},'daily'));
    console.log(ar);
}

console.log("Audit Record Generator, simple test on couchdb");
test();
