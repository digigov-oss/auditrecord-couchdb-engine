const generateAuditRecord = require('@digigov-oss/gsis-audit-record-db').default;
const { CouchDBEngine } = require('../dist/cjs/index.js');
const config = require('./config.json');

const test = async () => {
    const ar = await generateAuditRecord({}, new CouchDBEngine(config.connection,{},'daily'));
    console.log(ar);
}

console.log("Audit Record Generator, simple test on couchdb");
test();