"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _CouchDBEngine_dbname, _CouchDBEngine_connection, _CouchDBEngine_client, _CouchDBEngine_pnreset;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CouchDBEngine = void 0;
const nano_1 = __importDefault(require("nano"));
/**
 * @description AuditEngine implementation
 * @note This class is used to implement the methods that must be implemented by the AuditEngine
 * @class CouchDBEngine
 * @implements AuditEngine
 * @param {string} connectionString - connect to db via connection string `http://user:pass@$dbHost:dbPort`
 * @param {DatabaseSettings} dbSettings - database settings (please note the tableName is actualy a DB name for couchdb)
 */
class CouchDBEngine {
    couch() {
        return __awaiter(this, void 0, void 0, function* () {
            const nano = (0, nano_1.default)(__classPrivateFieldGet(this, _CouchDBEngine_connection, "f"));
            const dbList = yield nano.db.list(); // list all databases
            try {
                if (!dbList.includes(__classPrivateFieldGet(this, _CouchDBEngine_dbname, "f"))) {
                    // create a new DB if database doesn't exist.
                    yield nano.db.create(__classPrivateFieldGet(this, _CouchDBEngine_dbname, "f"));
                    const db = nano.use(__classPrivateFieldGet(this, _CouchDBEngine_dbname, "f"));
                    console.log("database created successfully");
                    return db;
                }
                else {
                    const db = nano.use(__classPrivateFieldGet(this, _CouchDBEngine_dbname, "f"));
                    return db;
                }
            }
            catch (err) {
                throw new Error(err);
            }
        });
    }
    ;
    /**
     * @description constructor
     * @param {string} connectionString - connect to couchdb via connection string
     * @param {DatabaseSettings} settings - settings for the database
     * @memberof CouchDBEngine
     */
    constructor(connectionString = "", dbSettings = {}, pnreset = "innumerable") {
        _CouchDBEngine_dbname.set(this, void 0);
        _CouchDBEngine_connection.set(this, void 0);
        _CouchDBEngine_client.set(this, void 0);
        _CouchDBEngine_pnreset.set(this, void 0);
        __classPrivateFieldSet(this, _CouchDBEngine_connection, connectionString != "" ? connectionString : "http://admin:admin@localhost:5984", "f");
        __classPrivateFieldSet(this, _CouchDBEngine_dbname, dbSettings.tableName || "audit_records", "f"); //this is actualy a DB name
        const init = () => __awaiter(this, void 0, void 0, function* () {
            return yield this.couch();
        });
        __classPrivateFieldSet(this, _CouchDBEngine_client, init(), "f");
        __classPrivateFieldSet(this, _CouchDBEngine_pnreset, pnreset, "f");
    }
    /**
     * @description Store a record in the database
     * @param {AuditRecord} record - record to be stored
     * @returns {AuditRecord} - the record stored
     * @memberof FileEngine
     * @method put
     */
    put(record) {
        return __classPrivateFieldGet(this, _CouchDBEngine_client, "f").then((db) => __awaiter(this, void 0, void 0, function* () {
            try {
                const doc = Object.assign({ _id: record.auditTransactionId }, record);
                const _rdoc = yield db.insert(doc);
                return record;
            }
            catch (error) {
                throw error;
            }
        }));
    }
    /**
     * @description Get a record from the database
     * @param auditTransactionId: string - transaction id
     * @returns {AuditRecord}
     * @memberof FileEngine
     * @method get
     */
    get(auditTransactionId) {
        return __classPrivateFieldGet(this, _CouchDBEngine_client, "f").then((db) => __awaiter(this, void 0, void 0, function* () {
            try {
                const rdoc = yield db.get(auditTransactionId);
                return rdoc;
            }
            catch (error) {
                throw error;
            }
        }));
    }
    /**
     * @description Generate a new sequence number
     * @param path
     * @returns number
     * @memberof CouchDBEngine
     * @method seq
     */
    seq() {
        // from update_seq get the numeric part and add 1
        return __classPrivateFieldGet(this, _CouchDBEngine_client, "f").then((db) => __awaiter(this, void 0, void 0, function* () {
            try {
                const rdoc = yield db.info();
                //get integer part from 1-xxxx
                const seq = parseInt(rdoc.update_seq.split("-")[0]);
                return seq + 1;
            }
            catch (error) {
                throw error;
            }
        }));
    }
    /**
     * @description Generate a new protocol number
     * @param path
     * @returns string
     * @memberof CouchDBEngine
     * @method protocol
     */
    pn() {
        return __classPrivateFieldGet(this, _CouchDBEngine_client, "f").then((db) => __awaiter(this, void 0, void 0, function* () {
            const pnreset = __classPrivateFieldGet(this, _CouchDBEngine_pnreset, "f");
            try {
                let protocol_split = "aion";
                let protocol_date = new Date().toISOString().split('T')[0];
                switch (pnreset) {
                    case "daily":
                        protocol_split = protocol_date;
                        break;
                    case "monthly":
                        protocol_split = protocol_date.split('-')[0] + "-" + protocol_date.split('-')[1];
                        break;
                    case "yearly":
                        protocol_split = protocol_date.split('-')[0];
                        break;
                    case "innumerable":
                        protocol_split = "aion";
                        break;
                }
                //Create indexes based on pnreset daily, monthly, yearly, innumerable
                const seqName = "prot_" + protocol_split.replace(/-/g, '');
                //create document in couchdb
                let doc = {
                    _id: seqName,
                    rev: "1"
                };
                //get the document
                try {
                    doc = yield db.get(seqName);
                }
                catch (error) {
                    //document doesn't exist
                }
                doc = yield db.insert(doc);
                const protocol = parseInt(doc['rev'].split("-")[0]);
                return protocol + "/" + protocol_date;
            }
            catch (error) {
                throw error;
            }
        }));
    }
}
exports.CouchDBEngine = CouchDBEngine;
_CouchDBEngine_dbname = new WeakMap(), _CouchDBEngine_connection = new WeakMap(), _CouchDBEngine_client = new WeakMap(), _CouchDBEngine_pnreset = new WeakMap();
exports.default = CouchDBEngine;
