"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CouchDBEngine = void 0;
var CouchDBEngine_js_1 = require("./couchdb/CouchDBEngine.js");
Object.defineProperty(exports, "CouchDBEngine", { enumerable: true, get: function () { return __importDefault(CouchDBEngine_js_1).default; } });
