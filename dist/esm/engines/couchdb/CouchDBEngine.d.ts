import { AuditRecord, AuditEngine, DatabaseSettings, PNRESETTYPES } from '@digigov-oss/gsis-audit-record-db';
/**
 * @description AuditEngine implementation
 * @note This class is used to implement the methods that must be implemented by the AuditEngine
 * @class CouchDBEngine
 * @implements AuditEngine
 * @param {string} connectionString - connect to db via connection string `http://user:pass@$dbHost:dbPort`
 * @param {DatabaseSettings} dbSettings - database settings (please note the tableName is actualy a DB name for couchdb)
 */
export declare class CouchDBEngine implements AuditEngine {
    #private;
    private couch;
    /**
     * @description constructor
     * @param {string} connectionString - connect to couchdb via connection string
     * @param {DatabaseSettings} settings - settings for the database
     * @memberof CouchDBEngine
     */
    constructor(connectionString?: string, dbSettings?: DatabaseSettings, pnreset?: PNRESETTYPES);
    /**
     * @description Store a record in the database
     * @param {AuditRecord} record - record to be stored
     * @returns {AuditRecord} - the record stored
     * @memberof FileEngine
     * @method put
     */
    put(record: AuditRecord): AuditRecord;
    /**
     * @description Get a record from the database
     * @param auditTransactionId: string - transaction id
     * @returns {AuditRecord}
     * @memberof FileEngine
     * @method get
     */
    get(auditTransactionId: string): AuditRecord;
    /**
     * @description Generate a new sequence number
     * @param path
     * @returns number
     * @memberof CouchDBEngine
     * @method seq
     */
    seq(): number;
    /**
     * @description Generate a new protocol number
     * @param path
     * @returns string
     * @memberof CouchDBEngine
     * @method protocol
     */
    pn(): string;
}
export default CouchDBEngine;
